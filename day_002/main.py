print("Welcome to the tip calculator.")

bill: float = float(input("What was the total bill? $").replace(",", "."))
people: int = int(input("How many people to split the bill? "))
tip: int = int(
    input("What percentage tip would you like to give? 10, 12 or 15? "))

tip_to_percentage: float = tip / 100
total_tip_amount: float = bill * tip_to_percentage
total_bill: float = bill + total_tip_amount
bill_per_person: float = total_bill / people

print(f"Each person should pay: ${bill_per_person:,.2f}")
